mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
current_dir := $(dir $(mkfile_path))

tags:
	ctags -R -f $(current_dir).git/tags $(current_dir)

run:
	python3 $(current_dir)bot.py
