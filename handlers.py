# for handling snippets and converting them to runnable code

class Handler:
    """Inheritable class for handlers"""
    def __init__(self, is_decorator=False, is_presnippet=False):
        self.is_decorator = is_decorator
        self.is_presnippet = is_presnippet
        self.code = []

    def get_code(self):
        return '\n'.join(self.code)


class Require(Handler):
    """takes one or more roles and produces a decorator"""
    def __init__(self, *args):
        super().__init__(is_decorator=True)
        self.code = ['@commands.has_any_role(']
        bld_str = ', '.join(args)
        self.code[0] += bld_str + ')'


class StrictRequire(Handler):
    """takes one or more roles and produces a series of decorators (has_role only accepts one role)"""
    def __init__(self, *args):
        super().__init__(is_decorator=True)
        prefix = '@commands.has_role('
        for role in args:
            self.code.append(prefix + role + ")")


class AddRoles(Handler):
    """takes a list of roles and adds them to the user that called the function"""
    def __init__(self, *args):
        super().__init__(is_presnippet=True)
        self.code = ['await internals.add_roles(ctx, ']
        bld_str = ', '.join(args)
        self.code[0] += bld_str + ')'


class RemoveRoles(Handler):
    """takes a list of roles and adds them to the user that called the function"""
    def __init__(self, *args):
        super().__init__(is_presnippet=True)
        self.code = ['await internals.remove_roles(ctx, ']
        bld_str = ', '.join(args)
        self.code[0] += bld_str + ')'


class Role(Handler):
    """takes a list of roles and adds them to the user that called the function"""
    def __init__(self, *args):
        super().__init__(is_presnippet=True)
        self.code = ['await internals.role(ctx, ']
        bld_str = ', '.join(args)
        self.code[0] += bld_str + ')'


class User(Handler):
    """sets up a call to the internals.mention_channel function"""
    def __init__(self):
        super().__init__()
        self.code = ["await internals.user(ctx)"]


class Channel(Handler):
    """sets up a call to the internals.mention_channel function"""
    def __init__(self, channel):
        super().__init__()
        self.code = ["await internals.mention_channel(ctx, '" + channel + "')"]


class NumVar(Handler):
    """for substituting cmd line args"""
    def __init__(self, n):
        super().__init__()
        # expects function to be defined as func(ctx, *, args: str)
        if n == 'N':
            self.code = ['args']
        elif n == '0':
            self.code = ['ctx.command.name']
        elif n.isdigit():
            self.code = ["(args.split()[" + n + "-1] if len(args.split()) >= " + n + " else '$" + n + "')"]
        else:
            self.code = ["'" + n + "'"]


class Default(Handler):
    """the default handler for functions which have no defined handler, always leaves code empty"""
    def __init__(self, *args, **kwargs):
        super().__init__()
        pass


def lookup_handler(func_name):
    return {
        'require': Require,
        'strict_require': StrictRequire,
        'add_roles': AddRoles,
        'remove_roles': RemoveRoles,
        'user': User,
        'role': Role,
        'numvar': NumVar,
        'channel': Channel
    }.get(func_name, Default)