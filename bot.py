import discord
import asyncio
# import requests
import logging
# import sqlite3
# import random

from config import loadconfig
from config import saveconfig
# from cogs.bumps import Bumps

from discord.ext import commands
from datetime import datetime
from icecream import ic

ic.disable()


# setup logger
logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('@%(name)s [%(levelname)s] %(asctime)s: %(message)s'))
logger.addHandler(handler)


# read in configuration file
conf = loadconfig()

# bump_tracker = Bumps()

# sqlite_version = bump_tracker.get_sqlite_version()

sleeping = False

default_cogs = ['basics', 'mod', 'owner', 'alias', 'events']  # , 'bumps', 'security']

# setup bot
bot = commands.Bot(command_prefix=conf['cmd_prefix'], description=conf['desc'], owner_id=conf['ownerID'])


# override on_ready
@bot.event
async def on_ready():
    print('/-----------------------------------------------------------------------------')
    print('| # ME')
    print('| Name:     ' + bot.user.name)
    print('| ID:       ' + str(bot.user.id))
    print('| Time:     ' + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    print('|-----------------------------------------------------------------------------')
    print('| # MODULES')
    # Import our 'modules'
    for cog in default_cogs:
        bot.load_extension('cogs.' + cog)
    global conf
    bot.load_extension('customs.customs')
    print('|-----------------------------------------------------------------------------')
    owner = await bot.fetch_user(bot.owner_id)
    from os import getpid
    await owner.send(f'ready ({getpid()})')


# override on_message
@bot.event
async def on_message(message):
    return

#     # do bump related things
#     if message.content[0] != bot.command_prefix:
#         global bump_tracker
#         await bump_tracker.on_message(message)

    # let the bot process the command
    # await bot.process_commands(message)


# override on_command_error
@bot.event
async def on_command_error(event, *args, **kwargs):
    if isinstance(args[0], discord.ext.commands.errors.MissingPermissions):
        author = event.message.author
        print('%s tried to run unauthorized command %s' % (author, event.command))
        await event.channel.send('%s you don\'t have permission to run that command' % author.mention)
    elif isinstance(args[0], discord.ext.commands.errors.NotOwner):
        author = event.message.author
        print('%s tried to run unauthorized command %s' % (author, event.command))
        await event.channel.send('%s you don\'t have permission to run that command, only the owner does' % author.mention)
    elif isinstance(args[0], discord.ext.commands.errors.CheckFailure):
        author = event.message.author
        print('%s tried to run unauthorized command %s' % (author, event.command))
        await event.channel.send('%s you don\'t have permission to run that command' % author.mention)
    else:
        print(args[0])
        raise args[0]

# override join event

# override leave event

# override ban event

# override message delete event


# global check for sleeping
@bot.check
def sleeping_check(ctx):
    global sleeping
    global conf
    ic(sleeping, ctx.command.name)
    if sleeping is True:
        if ctx.author.id == conf['ownerID'] and ctx.command.name == 'awake':
            sleeping = False
            return True
        else:
            return False
    else:
        if ctx.author.id == conf['ownerID'] and ctx.command.name == 'sleep' and ctx.message.content[1:] == 'sleep':
            sleeping = True
            return True
        else:
            return True


# Tasks

# check for ending temporary bans
async def ban_cleanup():
    await bot.wait_until_ready()

    while not bot.is_closed():
        print('cleaning up bans')
        tempbans = loadconfig('tempbans.json')
        remove = []
        for uid_sid in tempbans.keys():
            end_time = tempbans.get(uid_sid, {}).get('endTime', 0)
            print('evaluating %s | end time = %s | current time = %s' % (uid_sid, end_time, datetime.now().timestamp()))
            if datetime.now().timestamp() > end_time:
                uid, sid = uid_sid.split('_')
                g = bot.get_guild(sid)
                if g is None:
                    continue
                user = await bot.get_user_info(uid)
            await g.unban(user, reason='ban time is up')
            remove.append(uid_sid)
        for uid_sid in remove:
            tempbans.pop(uid_sid, None)
            print('%s | unbanning %s' % (datetime.now().strftime("%Y-%m-%d %H:%M:%S"), uid_sid))
        saveconfig(tempbans, 'tempbans.json')

        await asyncio.sleep(60 * 60)


# check for ending temporary mutes
async def mute_cleanup():
    await bot.wait_until_ready()

    while not bot.is_closed():
        print('%s | cleaning mutes' % datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        tempmutes = loadconfig('tempmutes.json')
        remove = []
        for uid_sid in tempmutes.keys():
            end_time = tempmutes.get(uid_sid, {}).get('endTime', 0)
            print('evaluating %s | end time = %s | current time = %s (%s)' % (uid_sid, end_time, datetime.now().timestamp(), datetime.now().strftime("%Y-%m-%d %H:%M:S")))
            if datetime.now().timestamp() > end_time:
                uid, sid = uid_sid.split('_')
                conf = loadconfig()
                mute_role = conf.get('security').get('mute_role', '')
                full_mute_role = conf.get('security').get('full_mute_role', '')
                glds = bot.guilds
                for gld in glds:
                    if str(gld.id) == sid:
                        g = gld
                        break
                else:
                    continue
                user = g.get_member(int(uid))
                if user is None:
                    continue
                for r in g.roles:
                    if mute_role == r.name and r in user.roles:
                        await user.remove_roles(*[r])
                        break
                for r in g.roles:
                    if full_mute_role == r.name and r in user.roles:
                        await user.remove_roles(*[r])
                        break
                remove.append(uid_sid)
        for uid_sid in remove:
            tempmutes.pop(uid_sid, None)
            print('%s | unmuting %s' % (datetime.now().strftime("%Y-%m-%d %H:%M:%S"), uid_sid))
        saveconfig(tempmutes, 'tempmutes.json')

        await asyncio.sleep(60 * 5)


# launch bot
if __name__ == '__main__':
    # get secret key
    secret = ''
    with open(conf['token_fname'], 'r') as fp:
        secret = fp.readline().strip()
    #bot.loop.create_task(ban_cleanup())
    #bot.loop.create_task(mute_cleanup())
    bot.run(secret)
