import handlers

from lark import Lark
from lark import Transformer
from lark import lexer


class CCCompiler(Transformer):
    def __init__(self, name):
        super().__init__()
        self.name = name
        self.decorators = ["@commands.command(description='{} custom command')".format(self.name)]
        self.sig_starts = ["", "#START {}".format(self.name), ""]
        self.sig_ends = ["", "#STOP {}".format(self.name), ""]
        self.presnippets = []
        self.format_str = ''
        self.format_code = []
        self.tab = ' '*4

    def program(self, txt):
        prog = []
        indent = 1

        for line in self.sig_starts:
            temp = self.tab*indent
            temp += line
            prog.append(temp)

        for line in self.decorators:
            temp = self.tab*indent
            temp += line
            prog.append(temp)

        temp = self.tab*indent
        temp += 'async def {}(self, ctx, *, args: str=""):'.format(self.name)
        prog.append(temp)

        indent += 1

        for line in self.presnippets:
            temp = self.tab*indent
            temp += line
            prog.append(temp)

        temp = self.tab*indent
        temp += 'await ctx.channel.send("' + self.format_str.strip() + '".format(' + ', '.join(self.format_code) + '))'
        prog.append(temp)

        indent -= 1
        for line in self.sig_ends:
            temp = self.tab*indent
            temp += line
            prog.append(temp)

        return '\n'.join(prog)

    def chunk(self, txt):
        #print(type(txt), txt)
        if isinstance(txt[0], handlers.NumVar):
            self.format_str += '{}'
            self.format_code.append(txt[0].get_code())
            return txt[0].get_code()
        elif isinstance(txt[0], handlers.Channel):
            self.format_str += '{}'
            self.format_code.append(txt[0].get_code())
            return txt[0].get_code()
        elif isinstance(txt[0], handlers.Handler):
            self.format_str += '{}'
            self.format_code.append(txt[0].get_code())
            return txt[0].get_code()
        return txt[0]

    def snippet(self, txt):
        #print(type(txt), txt)
        # need to filter out whitespace tokens
        res = []
        for i, a in enumerate(txt):
            if isinstance(a, lexer.Token) and a.type == 'WS':
                pass
            else:
                res.append(a)
        # print(type(res[0]), res[0])
        if len(res) > 1:
            h = handlers.lookup_handler(res[0])(*res[1:][0])
        else:
            h = handlers.lookup_handler(res[0])()

        if h.is_decorator:
            for line in h.code:
                self.decorators.append(line)
            return None
        elif h.is_presnippet:
            for line in h.code:
                self.presnippets.append(line)
            return None
        return h
        # return 5

    def command(self, txt):
        #print(type(txt), txt)
        # if cmd[0] == 'require':
        #     print('user:', self.ctx)
        return txt[0].value

    def args(self, txt):
        #print(type(txt), txt)
        # args nominally recieves a bunch of strings
        # if it receives anything else it should handle that specially
        # special cases:
        #   WS: drop
        #   None: drop
        #   Handler class instance: pass on code
        res = []
        for i, a in enumerate(txt):
            if a == None:
                pass
            elif isinstance(a, handlers.Handler):
                res.append(a.get_code())
            elif isinstance(a, lexer.Token) and a.type == 'WS':
                pass
            else:
                res.append("'" + str(a).strip() + "'")
        #print(res)
        return res

    def arg(self, txt):
        #print(type(txt[0]), txt[0])
        return txt[0]

    def channel(self, txt):
        #print(type(txt), txt)
        h = handlers.lookup_handler('channel')(txt[0].value)
        return h
        # return txt[0].value

    def numvar(self, txt):
        #print(type(txt), txt)
        h = handlers.lookup_handler('numvar')(txt[0].value)
        return h
        # return txt[0].value

    def text(self, txt):
        #print(type(txt[0].value), txt[0].value)
        self.format_str += txt[0].value
        return txt[0].value

    def letters(self, arg):
        # print(type(arg[0].value), arg[0].value)
        return arg[0].value


class Compiler:
    """compiles custom commands"""
    def __init__(self):
        self.dis_grammar = r'''
            program: chunk+

            chunk  : snippet
                   | text
                   | channel
                   | numvar

            snippet: "{" [ WS* command WS* (":" WS* args  WS*)?] "}"

            command: LETTERS

            args   : [arg ("," WS* arg)*]

            arg    : snippet
                   | /[^\{^\}^$^,^\t^\r^\n^\f]+/
                   | numvar

            LETTERS: /\w+/

            channel: "#" /[a-zA-Z0-9_\-]+/

            numvar : "$" (/[0-9]+/|/[N]/)

            text   : /[^\{^\}^#^$]+?/

            %import common.WS
            '''
        self.dis_parser = Lark(self.dis_grammar, start='program')

    def compile(self, name, custom_command):
        # returns compiled version of program or (None, e) if compilation failed
        try:
            tree = self.dis_parser.parse(custom_command)
            # DEBUG
            print(name + ':\n', tree.pretty())
            ccc = CCCompiler(name)
            return ccc.transform(tree)
        except Exception as e:
            #raise e
            return (None, e)


if __name__ == '__main__':
    text = '{require: captain, officer, {user}} o7 #channel_name $1 {user}'
    text2 = '{require: a, b, c}'
    text3 = "{role: +AvatarMC}\r\n{user} has become a minecrafter!"
    text4 = "{strict_require: Advanced Bender, Airbender}\r\n{role: +Projection, -Weightless}\r\n{user} has mastered the art of projection!"
    text5 = "{role: +Waterbender, -Earthbender, -Firebender, -Airbender, -Nonbender, -Lightningbender, -Combustionbender, -Weightless, -Projection, -Lavabender, -Metalbender, -Chiblocker}\r\n{user} has chosen the element of water!"
    text6 = '{require: Captain, Officer}o7 #general $1 {user}'
    text7 = '{strict_require: Captain, Officer}o7 #general $N {user}'
    c = Compiler()
    print(c.compile('water', text7))
    # tree = dis_parser.parse(text5)
    # print(dis_parser.parse(text5).pretty())

    # myt = CCCompiler('water')
    # myt.ctx = 'bob'

    # print(myt.transform(tree))