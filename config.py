import json


def loadconfig(fname='config.json'):
    conf = {}   
    with open(fname, 'r') as fp:
        conf = json.load(fp)
    return conf


def saveconfig(conf, fname='config.json'):
    with open(fname, 'w') as fp:
        json.dump(conf, fp, sort_keys=True, indent=4)