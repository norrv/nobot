import requests
import discord
import json
import re

from config import loadconfig

from icecream import ic
from discord.ext import commands


class Owner(commands.Cog):
    """commands for mod use only"""
    def __init__(self, bot):
        self.bot = bot
        self.conf = loadconfig('config.json')
        print('| Loaded:   owner')


    @commands.command(description='sleeps bot')
    @commands.is_owner()
    async def sleep(self, ctx, ver: str=''):
        if ver == '' or ver == 'v2':
            await ctx.channel.send('Sleeping')


    @commands.command(description='wakes bot')
    @commands.is_owner()
    async def awake(self, ctx, ver: str=''):
        if ver == '' or ver == 'v2':
            await ctx.channel.send('Waking up')


    @commands.command(description='kills bot')
    @commands.is_owner()
    async def kill(self, ctx, ver: str=''):
        if ver == '' or ver == 'v2':
            await ctx.channel.send('bye')
            exit(2)


    @commands.command(description='sends pid')
    @commands.is_owner()
    async def pid(self, ctx, ver: str=''):
        if ver == '' or ver == 'v2':
            from os import getpid
            await ctx.channel.send(getpid())


    @commands.command(description='sends ip')
    @commands.is_owner()
    async def ip(self, ctx, ver: str=''):
        if ver == '' or ver == 'v2':
            r = requests.get('http://checkip.dyndns.org')
            vm_ip = re.findall(r'\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}', r.text)
            if (len(vm_ip) == 1):
                await ctx.channel.send(vm_ip[0])
            else:
                await ctx.channel.send('problem finding ip')


def setup(bot):
    bot.add_cog(Owner(bot))
