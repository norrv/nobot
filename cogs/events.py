import discord
import compiler
import json
import requests
import re

from config import loadconfig
from config import saveconfig

from icecream import ic
from discord.ext import commands

ic.disable()


class Events(commands.Cog):
    """commands for mod use only"""
    def __init__(self, bot):
        self.bot = bot
        self.conf = loadconfig('config.json')
        self.new_roles = self.conf.get('new_roles', [])
        print('| Loaded:   events')


    def check_mod_perms(ctx):
        return ctx.author.guild_permissions.kick_members


    def check_admin_perms(ctx):
        return ctx.author.guild_permissions.administrator


    async def on_member_join(self, member):
        # assign roles
        ic(self.new_roles)
        roles = []
        for r in self.bot.guilds[0].roles:
            if r.name in self.new_roles and r not in roles:
                roles.append(r)
        ic(roles)
        await member.edit(roles=roles)


    @commands.group(pass_context=True)
    @commands.check(check_admin_perms)
    async def welcome(self, ctx):
        """group command testing"""
        if ctx.invoked_subcommand is None:
            pass


    @welcome.command(name='add_roles')
    async def _add_roles(self, ctx, delim: str, *, roles: str):
        roles = [r.strip() for r in roles.split(delim)]
        for r in roles:
            if r not in self.new_roles:
                self.new_roles.append(r)
        loadconfig()
        self.conf['new_roles'] = self.new_roles
        saveconfig(self.conf)
        await ctx.channel.send("%s: roles added" % ctx.author.mention)


    @welcome.command(name='add_role')
    async def _add_role(self, ctx, role: str):
        if role not in self.new_roles:
            self.new_roles.append(role)
        loadconfig()
        self.conf['new_roles'] = self.new_roles
        saveconfig(self.conf)
        await ctx.channel.send("%s: role added" % ctx.author.mention)


    @welcome.command(name='remove_roles')
    async def _remove_roles(self, ctx, delim: str, *, roles: str):
        roles = [r.strip() for r in roles.split(delim)]
        final_roles = []
        for r in self.new_roles:
            if r not in roles:
                final_roles.append(r)
        loadconfig()
        self.new_roles = final_roles
        self.conf['new_roles'] = self.new_roles
        saveconfig(self.conf)
        await ctx.channel.send("%s: roles removed" % ctx.author.mention)


    @welcome.command(name='remove_role')
    async def _remove_role(self, ctx, role: str):
        final_roles = []
        for r in self.new_roles:
            if r != role:
                final_roles.append(r)
        loadconfig()
        self.new_roles = final_roles
        self.conf['new_roles'] = self.new_roles
        saveconfig(self.conf)
        await ctx.channel.send("%s: role removed" % ctx.author.mention)


    @welcome.command(name='set_roles')
    async def _set_roles(self, ctx, delim: str, *, roles: str):
        self.new_roles = [r.strip() for r in roles.split(delim)]
        loadconfig()
        self.conf['new_roles'] = self.new_roles
        saveconfig(self.conf)
        await ctx.channel.send("%s: roles set" % ctx.author.mention)


    @welcome.command(name='list_roles')
    async def _list_roles(self, ctx):
        bld_str = "%s: current auto assigned roles\n```\n" % ctx.author.mention
        bld_str += '\n'.join(self.new_roles) + '\n```'
        await ctx.channel.send(bld_str)


def setup(bot):
    bot.add_cog(Events(bot))
