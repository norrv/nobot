import discord
import asyncio
import sqlite3
import random

from discord.ext import commands
from datetime import datetime
from config import loadconfig
from icecream import ic

ic.disable()


# class for keeping track of bumps
class Bumps(commands.Cog):
    """class for keeping track of bumps"""
    def __init__(self, bot):
        self.bot = bot
        self.bot.add_listener(self.on_message_listener, 'on_message')
        self.conf = loadconfig()
        self.last_bumper = None
        self.last_bump = 0
        self.db = None
        self.sqlite_version = '???'
        self.emojis = ['💃', '😎', '🙏', '🙌', '👏', '👍', '😁']

        try:
            self.db = sqlite3.connect(self.conf['bumpsDB_path'])
            with self.db:
                c = self.db.cursor()
                c.execute('SELECT SQLITE_VERSION()')
                data = c.fetchone()
                self.sqlite_version = data[0]
        except Exception as e:
            raise e

        try:
            with open(self.conf['last_bump_path'], 'r') as fp:
                self.last_bump = int(fp.readline().strip())
        except Exception as e:
            print('Error loading last bump', str(e))
            last_bump = int(datetime.now().timestamp())

    def get_sqlite_version(self):
        return self.sqlite_version


    async def on_message_listener(self, message):
        if message.content.startswith('=bump'):
            self.last_bumper = message.author
            print('last bumper: %s' % message.author.display_name)
        elif self.last_bumper is not None and message.content.startswith('Bumped') and message.author.id == self.conf['serverhoundID']:
            if int(datetime.now().timestamp()) < self.last_bump + 1800:
                return
            else:
                print('saw bump, current last bumper is %s' % self.last_bumper.display_name)
                self.last_bump = int(datetime.now().timestamp())
                try:
                    with open(self.conf['last_bump_path'], 'w') as fp:
                        fp.write(str(self.last_bump))
                except Exception as e:
                    print('Error writing last bump time:', str(e))
                await self.bumped(message)


    async def bumped(self, message):
        with self.db:
            # Let's get informations about our user in this server...
            db_cur = self.db.cursor()
            db_cur.execute("SELECT bumps FROM bumpers WHERE userId=? AND serverId=?", (self.last_bumper.id, message.guild.id))
            row = db_cur.fetchone()
            # Did that person bump once?
            if row is None:
                # If not, add it to the DB with the value of "1"
                db_cur.execute("INSERT INTO bumpers(userId, serverId, bumps) VALUES(?, ?, 1)", (self.last_bumper.id, message.guild.id))
                bump_score = 'This is your first bump here! You can use `-bumps` to see your score.'
            else:
                # If yes, increment value
                c = int(row[0]) + 1
                db_cur.execute("UPDATE bumpers SET bumps=? WHERE userId=? AND serverId=?", (c, self.last_bumper.id, message.guild.id))
                # And let's display a nice message
                if c == 2:
                    bump_score = 'This is your second bump, keep going!'
                elif c == 3:
                    bump_score = 'A third bump? How nice of you. :)'
                elif c == 10:
                    bump_score = 'This is your tenth bump! Woo!'
                elif c == 25:
                    bump_score = 'You bumped this server 25 times! Pretty impressive.'
                elif c == 50:
                    bump_score = 'You bumped this server 50 times, time to hit the hundredth!'
                elif c == 69:
                    bump_score = 'You bumped this server 👀 times.'
                elif c == 90:
                    bump_score = 'You bumped this server 90 times.\n _Bumping in the 90\'s_ 🎶\nhttps://www.youtube.com/watch?v=XCiDuy4mrWU'
                elif c == 100:
                    bump_score = 'Behold. A new **bump master** arrived, with 100 bumps to their score!'
                elif c == 150:
                    bump_score = 'Wow! 150 bumps? You\'re never giving this up, are you?\nhttps://www.youtube.com/watch?v=dQw4w9WgXcQ'
                elif c == 200:
                    bump_score = 'Dude what. 200 times? That\'s determination.'
                elif c == 300:
                    bump_score = 'THIS. IS. SPARTA!'
                elif c == 301:
                    bump_score = 'The hell are you doing???'
                elif c == 302:
                    bump_score = 'Like, stop.'
                elif c == 303:
                    bump_score = 'How did you bump this much?'
                elif c == 304:
                    bump_score = 'Welp. 304'
                elif c == 305:
                    bump_score = 'And 305...'
                elif c == 306:
                    bump_score = 'To get to this number of bumps (306), you waited **AT LEAST** 1224 hours, that\'s 51 days.'
                elif c == 307:
                    bump_score = '307. You\'re leaving me depressed.'
                elif c == 308:
                    bump_score = 'Aaaaaaaand 308.'
                elif c == 309:
                    bump_score = 'Here comes 309.'
                elif c == 310:
                    bump_score = 'Alright, that\'s 310. Let\'s get more custom messages at 480.'
                elif c == 480:
                    bump_score = 'Hello there! Did you know this is your 480th bump? That means your **minimal** wait time is 80 days. ' +\
                                 'And since I think you didn\'t really bump every four hours, that means you\'ve been here for a ' +\
                                 'looooooong time. _Please, tell me your secret._'
                elif c == 500:
                    bump_score = 'Hey, 500 bumps! Please go see norv for a surprise.'
                elif c == 1000:
                    bump_score = 'Ayy 1000 bumps! Please go see norv and tell him to shut me down.'
                else:
                    bump_score = 'You bumped this server %s times.' % (str(c))
        await message.channel.send('Thank you, %s! %s %s' % (self.last_bumper.mention, bump_score, random.choice(self.emojis)))


def setup(bot):
    bot.add_cog(Bumps(bot))
