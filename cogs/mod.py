import discord
import compiler
import requests
import re
import subprocess

from config import loadconfig
from config import saveconfig

from icecream import ic
from discord.ext import commands
from datetime import datetime, timedelta

ic.disable()


# TODO: log actions


class Mod(commands.Cog):
    """commands for mod use only"""
    def __init__(self, bot):
        self.bot = bot
        self.conf = loadconfig('config.json')
        self.end_funcs_marker = '#END_FUNCS_MARKER - DO NOT REMOVE'

        print('| Loaded:   mod')


    def check_mod_perms(ctx):
        return ctx.author.guild_permissions.kick_members


    def check_admin_perms(ctx):
        return ctx.author.guild_permissions.administrator


    async def mute(self, user, guild, reason=''):
        mute_role = self.conf.get('security').get('mute_role', '')
        for r in guild.roles:
            if mute_role == r.name and r not in user.roles:
                await user.add_roles(*[r])
                break


    async def fullmute(self, user, guild, reason=''):
        mute_role = self.conf.get('security').get('full_mute_role', '')
        for r in guild.roles:
            if mute_role == r.name and r not in user.roles:
                await user.add_roles(*[r])
                break


    async def unmute(self, user, guild, reason=''):
        mute_role = self.conf.get('security').get('mute_role', '')
        for r in guild.roles:
            if mute_role == r.name and r in user.roles:
                await user.remove_roles(*[r])
                break

    async def unfullmute(self, user, guild, reason=''):
        mute_role = self.conf.get('security').get('full_mute_role', '')
        for r in guild.roles:
            if mute_role == r.name and r in user.roles:
                await user.remove_roles(*[r])
                break


    @commands.group(pass_context=True)
    @commands.check(check_mod_perms)
    async def mod(self, ctx):
        """mod commands group"""
        if ctx.invoked_subcommand is None:
            pass


    @mod.command(name='ban', description='ban a user')
    async def _ban(self, ctx, user: discord.Member, *, reason: str=''):
        """
        Usage: mod ban user [reason]
        """
        await user.ban(reason=reason)
        await ctx.channel.send("%s: done" % ctx.author.mention)


    #@mod.command(name='unban', description='unban a user, must use user ID')
    #async def _unban(self, ctx, user_id: str, *, reason: str=''):
        #"""
        #Usage: mod unban user_id [reason]
        #"""
        #user = await self.bot.get_user_info(user_id)
        #server_id = ctx.message.guild.id
        #if user is None:
            #await ctx.channel.send("%s: couldn't find user with id %s" % (ctx.author.mention, user_id))
        #else:
            #tempbans = loadconfig('tempbans.json')
            #if tempbans.pop(str(user_id) + '_' + str(server_id), None) is not None:
                #saveconfig(tempbans, 'tempbans.json')
            #await ctx.guild.unban(user, reason=reason)
            #await ctx.channel.send("%s: done" % ctx.author.mention)


    #@mod.command(name='tempban', description='temporarily ban a user, default is 24hrs')
    #async def _temp_ban(self, ctx, user: discord.Member, timespan: int=24, *, reason: str=''):
        #"""
        #Usage: mod tempban user timespan_in_hours [reason]
        #timespan is in hours
        #"""
        #user_id = user.id
        #server_id = ctx.message.guild.id
        #tempbans = loadconfig('tempbans.json')
        #if tempbans.get(str(user_id) + '_' + str(server_id), {}).get('endTime', None) is None:
            #await user.ban(reason=reason)
            #tempbans[str(user_id) + '_' + str(server_id)] = {
                #'endTime': datetime.now().timestamp() + (timespan * 60 * 60),
                #'reason': reason
            #}
            #saveconfig(tempbans, 'tempbans.json')
            #await ctx.channel.send("%s: done" % ctx.author.mention)
        #else:
            ## If yes, quit
            #await ctx.channel.send("%s: that user is already temporarily banned" % ctx.author.mention)


    @mod.command(name='mute', description='mute a user')
    async def _mute(self, ctx, user: discord.Member, *, reason: str=''):
        """
        Usage: mod mute user [reason]
        """
        tempmutes = loadconfig('tempmutes.json')
        await self.unfullmute(user, ctx.guild, reason=reason)
        await self.mute(user, ctx.guild, reason=reason)
        user_id = user.id
        server_id = ctx.message.guild.id
        key = str(user_id) + '_' + str(server_id)
        tempmutes[key] = tempmutes.get(key, {
            'endTime': None,
            'reason': reason
        })
        saveconfig(tempmutes, 'tempmutes.json')
        await ctx.channel.send("%s: done" % ctx.author.mention)


    @mod.command(name='fullmute', description='fully mute a user')
    async def _fullmute(self, ctx, user: discord.Member, *, reason: str=''):
        """
        Usage: mod fullmute user [reason]
        """
        tempmutes = loadconfig('tempmutes.json')
        await self.unmute(user, ctx.guild, reason=reason)
        await self.fullmute(user, ctx.guild, reason=reason)
        user_id = user.id
        server_id = ctx.message.guild.id
        key = str(user_id) + '_' + str(server_id)
        tempmutes[key] = tempmutes.get(key, {
            'endTime': None,
            'reason': reason
        })
        saveconfig(tempmutes, 'tempmutes.json')
        await ctx.channel.send("%s: done" % ctx.author.mention)


    @mod.command(name='unmute', description='unmute a user')
    async def _unmute(self, ctx, user: discord.Member, *, reason: str=''):
        """
        Usage: mod unmute user [reason]
        """
        user_id = user.id
        server_id = ctx.message.guild.id
        await self.unmute(user, ctx.guild, reason)
        await self.unfullmute(user, ctx.guild, reason)
        tempmutes = loadconfig('tempmutes.json')
        if tempmutes.pop(str(user_id) + '_' + str(server_id), None) is not None:
            saveconfig(tempmutes, 'tempmutes.json')
        await ctx.channel.send("%s: done" % ctx.author.mention)


    @mod.command(name='tempmute', description='temporarily mute a user, default is 24hrs')
    async def _temp_mute(self, ctx, user: discord.Member, timespan: int=24, *, reason: str=''):
        """
        Usage: mod tempmute user timespan_in_hours [reason]
        timespan is in hours
        """
        user_id = user.id
        server_id = ctx.guild.id
        tempmutes = loadconfig('tempmutes.json')
        if tempmutes.get(str(user_id) + '_' + str(server_id), {}).get('endTime', None) is None:
            await self.mute(user, ctx.guild, reason=reason)
            endtime = datetime.now() + timedelta(seconds=timespan * 60 * 60)
            tempmutes[str(user_id) + '_' + str(server_id)] = {
                'endTime': endtime.timestamp(),
                'reason': reason
            }
            saveconfig(tempmutes, 'tempmutes.json')
            await ctx.channel.send("%s: done, user will be muted for %s hours until %s (%s)" % (ctx.author.mention, timespan, endtime.timestamp(), endtime.strftime("%Y-%m-%d %H:%M:%S")))
        else:
            # If yes, quit
            await ctx.channel.send("%s: that user is already temporarily muted" % ctx.author.mention)


    @mod.command(name='servertime', description='get the current unix timestamp and datetime on the server')
    async def _servertime(self, ctx):
        await ctx.channel.send('%s: %s (%s)' % (ctx.author.mention, datetime.now().timestamp(), datetime.now().strftime("%Y-%m-%d %H:%M:%S")))


    # purge commands and such
    @commands.group(pass_context=True)
    @commands.check(check_mod_perms)
    async def purge(self, ctx):
        """purge commands group, deletes at most <limit> number of messages, but does not gaurentee that many messages will be deleted"""
        if ctx.invoked_subcommand is None:
            def delete_all(m):
                return True
            content = ctx.message.content
            args = content.split()
            limit = 100
            if len(args) > 1:
                limit = int(args[1])
            if limit > 1000:
                limit = 1000
            await self.purge_helper(ctx.channel, limit, delete_all)


    # method that actually does the purging
    # takes a function as an argument that it applies to each message to determine if the message should be deleted
    async def purge_helper(self, channel, limit, should_delete):
        msgs = await channel.history(limit=1000).flatten()
        count = 0
        for m in msgs:
            if should_delete(m):
                await m.delete()
                count += 1
                if count > limit:
                    break


    @purge.command(name='any', description='Delete a number of messages from the current channel')
    async def _any(self, ctx, limit: int):
        def delete_all(m):
            return True

        await self.purge_helper(ctx.channel, limit, delete_all)


    @purge.command(name='user', description='Delete a number of messages for a user from the current channel')
    async def _user(self, ctx, limit: int, user: discord.Member):
        def func(m, uid=user.id):
            return m.author.id == uid

        if limit > 1000:
            limit = 1000
        await self.purge_helper(ctx.channel, limit, func)
        await ctx.message.delete()


    @purge.command(name='match', description='Delete a number of messages that contain a given string from the current channel')
    async def _match(self, ctx, limit: int, *, text: str):
        def func(m, text=text):
            return text in m.content

        if limit > 1000:
            limit = 1000
        await self.purge_helper(ctx.channel, limit, func)
        await ctx.message.delete()


    @purge.command(name='not', description='Delete a number of messages that do not contain a given string from the current channel')
    async def _not(self, ctx, limit: int, *, text: str):
        def func(m, text=text):
            return text not in m.content

        if limit > 1000:
            limit = 1000
        await self.purge_helper(ctx.channel, limit, func)
        await ctx.message.delete()


    @purge.command(name='startswith', description='Delete a number of messages that start with a given string from the current channel')
    async def _startswith(self, ctx, limit: int, *, text: str):
        def func(m, text=text):
            return m.content.startswith(text)

        if limit > 1000:
            limit = 1000
        await self.purge_helper(ctx.channel, limit, func)
        await ctx.message.delete()


    @purge.command(name='endswith', description='Delete a number of messages that ends with a given string from the current channel')
    async def _endswith(self, ctx, limit: int, *, text: str):
        def func(m, text=text):
            return m.content.endswith(text)

        if limit > 1000:
            limit = 1000
        await self.purge_helper(ctx.channel, limit, func)
        await ctx.message.delete()


    @purge.command(name='links', description='Delete a number of messages that contain a link from the current channel')
    async def _links(self, ctx, limit: int):
        p = re.compile(r"http[s]?://[a-z1-9./]+")
        def func(m, p=p):
            return p.search(m.content) is not None

        if limit > 1000:
            limit = 1000
        await self.purge_helper(ctx.channel, limit, func)
        await ctx.message.delete()


    @purge.command(name='invites', description='Delete a number of messages that contain invites from the current channel')
    async def _invites(self, ctx, limit: int):
        p = re.compile(r"http[s]?://discord.gg/[a-zA-Z0-9]+")
        def func(m, p=p):
            return p.search(m.content) is not None

        if limit > 1000:
            limit = 1000
        await self.purge_helper(ctx.channel, limit, func)
        await ctx.message.delete()


    @purge.command(name='images', description='Delete a number of messages that contain images from the current channel')
    async def _images(self, ctx, limit: int):
        def func(m):
            return len(m.attachments) > 0

        if limit > 1000:
            limit = 1000
        await self.purge_helper(ctx.channel, limit, func)
        await ctx.message.delete()


    @purge.command(name='mentions', description='Delete a number of messages that contain mentions from the current channel')
    async def _mentions(self, ctx, limit: int):
        def func(m):
            return len(m.mentions) > 0

        if limit > 1000:
            limit = 1000
        await self.purge_helper(ctx.channel, limit, func)
        await ctx.message.delete()




    @commands.group(pass_context=True)
    @commands.check(check_admin_perms)
    async def program(self, ctx):
        """custom program commands group"""
        if ctx.invoked_subcommand is None:
            pass


    def write_custom_func(self, func_str, fname='customs.py'):
        # first, read in the file line by line
        ic()
        code = ''
        with open('customs/' + fname, 'r') as f:
            for line in f:
                if self.end_funcs_marker in line:
                    code += func_str
                code += line

        ic(code)

        with open('customs/' + fname, 'w') as f:
            f.write(code)


    @program.command(name='add', description='add a custom command')
    async def add_program(self, ctx, name: str, url: str):
        """
        <url> is a pastebin url with the text of the custom command to add
        """
        # reload config
        self.conf = loadconfig('config.json')
        # check that name is allowed:
        if name in self.conf['illegal_names']:
            await ctx.channel.send('%s: the command name %s is not allowed' % (ctx.author.mention, name))
            return
        # url should point to a pastebin
        url = 'https://pastebin.com/raw/' + url[url.rfind('/') + 1:]  # paste key is everything past the last '/'
        r = requests.get(url)
        if r.status_code != 200 or r.text == 'Error with this ID!':
            await ctx.channel.send('Couldn\'t access url at ' + url + ' got return code: ' + str(r.status_code))
        else:
            # let's try and compile it
            c = compiler.Compiler()
            code = c.compile(name, r.text)
            if isinstance(code, tuple):
                await ctx.channel.send('%s: program %s failed to compile with error: %s' % (ctx.author.mention, name, code[1]))
            else:
                # write the code to a file
                self.write_custom_func(code)

                # load code
                self.bot.unload_extension('customs.customs')
                self.bot.load_extension('customs.customs')

                # add code to list of extensions to load
                if name not in self.conf['customs']:
                    self.conf['customs'].append(name)
                    saveconfig(self.conf)

                # save custom code
                raw_progs = loadconfig('progs.json')
                raw_progs[name] = r.text
                saveconfig(raw_progs, 'progs.json')
                await ctx.channel.send('%s: command "%s" added' % (ctx.author.mention, name))


    def remove_custom_func(self, func_name, fname='customs.py'):
        # read file in, skip custom part
        code = ''
        with open('customs/' + fname, 'r') as f:
            ignore = False
            for line in f:
                if not ignore and "#START {}".format(func_name) in line:
                    ignore = True
                    continue
                elif ignore and "#STOP {}".format(func_name) in line:
                    ignore = False
                    continue
                elif not ignore:
                    code += line

        with open('customs/customs.py', 'w') as f:
            f.write(code)


    @program.command(name='remove', description='remove a custom command, note that this doesn\'t actually delete the compiled file or the custom command code, but merely prevents the bot from loading it')
    async def remove_program(self, ctx, name: str):
        # reload config
        self.conf = loadconfig('config.json')
        if name not in self.conf['customs']:
            await ctx.channel.send('Program %s does not exist' % name)
        else:
            self.remove_custom_func(name)
            self.bot.unload_extension('customs.customs')
            self.bot.load_extension('customs.customs')
            self.conf['customs'].remove(name)
            saveconfig(self.conf)
            await ctx.channel.send('Program %s deleted' % name)


    @program.command(name='list', description='List the current programs')
    async def list_programs(self, ctx):
        # reload config
        self.conf = loadconfig('config.json')
        await ctx.channel.send('Here are the currently defined programs:\n```\n' + '\n'.join(self.conf['customs']) + '\n```')


    @program.command(name='read', description='read the contents of a program')
    async def read_program(self, ctx, name: str):
        # reload config
        self.conf = loadconfig('config.json')
        if name not in self.conf['customs']:
            await ctx.channel.send('the program %s does not exist' % name)
        else:
            bld_str = '```\n'
            raw_progs = loadconfig('progs.json')
            bld_str += raw_progs.get(name, 'no custom command code found')
            bld_str += '\n```\n' + ('=' * 10) + '\n' + 'compiled to:\n' + ('=' * 10) + '\n```\n'
            with open('customs/' + name + '.py', 'r') as fp:
                for line in fp:
                    bld_str += line
            bld_str += '```'
            await ctx.author.send(content=bld_str)
            await ctx.channel.send('The program should be in your DMs!')


    @commands.group(pass_context=True)
    @commands.check(check_admin_perms)
    async def admin(self, ctx):
        """admin only commands"""
        if ctx.invoked_subcommand is None:
            pass


    @admin.command(name='trivia', description='start trivia bot')
    async def start_trivia(self, ctx):
        subprocess.run(["../trivia_runner.sh"])
        await ctx.channel.send('%s: command run' % ctx.author.mention)


    @admin.command(name='force_quit', description='start trivia bot')
    async def force_quit(self, ctx):
        f = open('../can_start_appa.txt' , 'w')
        f.write('no')
        f.close()
        await ctx.channel.send('bye')
        exit(2)


    @commands.group(pass_context=True)
    @commands.check(check_mod_perms)
    async def msg(self, ctx):
        """custom program commands group"""
        if ctx.invoked_subcommand is None:
            pass


    @msg.command(name='print', description='Prints message to log')
    async def print_msg(self, ctx, msg_id: int):
        msg = None
        for guild in self.bot.guilds:
            for channel in guild.channels:
                try:
                    msg = await channel.fetch_message(msg_id)
                    if not isinstance(discord.Message, msg):
                        msg = None
                    break
                except:
                    pass

        if msg is None:
            await ctx.channel.send('%s: message id %s does not exist' % (ctx.author.mention, str(msg_id)))
            return
        else:
            print(msg.content)
            await ctx.channel.send('```\n%s\n```' % msg.content)


    @msg.command(name='post', description='Posts a message to a channel')
    async def post_msg(self, ctx, channel: str, *, words: str=''):
        if ctx.author.id == 329687040260046848:
            return
        # not using the discord.TextChannel so we can still send somewhere if they don't give us a channel
        sent = False
        for ch in ctx.guild.channels:
            if channel == ch.mention:
                await ch.send(words)
                sent = True
                continue
        if not sent:
            words = channel + ' ' + words
            await ctx.channel.send(words)


    @msg.command(name='fpost', description='Posts a message and any attached files to a channel')
    async def fpost_msg(self, ctx, channel: str='', *, words: str=''):
        if ctx.author.id == 329687040260046848:
            return
        # if channel not in ctx.guild.channels:
        #     await ctx.channel.send("%s: That channel doesn't exist" % ctx.author.mention)
        files = []
        fnames = []
        if len(ctx.message.attachments) > 0:
            for at in ctx.message.attachments:
                if at.filename in fnames:
                    i = 1
                    while at.filename + '_' + str(i) in fnames:
                        i += 1
                    fnames.append(at.filename + '_' + str(i))
                else:
                    fnames.append(at.filename)
                await at.save(fnames[-1])
                files.append(discord.File(open(fnames[-1], 'rb')))
        # await channel.send(content=words, files=files)
        sent = False
        for ch in ctx.guild.channels:
            if channel == ch.mention:
                await ch.send(content=words, files=files)
                sent = True
                continue
        if not sent:
            words = channel + ' ' + words
            await ctx.channel.send(content=words, files=files)


    async def _regex_sub_edit_msg(self, msg_txt, split_str):
        regex_str, repl_str, scope = split_str
        if scope.lower() == 'g':
            return re.sub(pattern=regex_str, repl=repl_str, string=msg_txt)
        elif scope.isdigit():
            return re.sub(pattern=regex_str, repl=repl_str, string=msg_txt, count=int(scope))
        else:
            return msg_txt


    async def _replace_edit_msg(self, msg_txt, split_str):
        match_str, repl_str, scope = split_str
        if scope.lower() == 'g':
            return msg_txt.replace(match_str, repl_str)
        elif scope.isdigit():
            return msg_txt.replace(match_str, repl_str, int(scope))
        else:
            return msg_txt


    async def _insert_edit_msg(self, msg_txt, split_str, start_ind=0):
        match_str, repl_str, scope = split_str
        index = msg_txt.find(match_str, start_ind)
        if index == -1:
            return msg_txt
        else:
            index += len(match_str)

        if scope.lower() == 'g':
            new_msg_txt = msg_txt[:index] + repl_str + msg_txt[index:]
            return await self._insert_edit_msg(new_msg_txt, [match_str, repl_str, scope], index + len(repl_str))
        elif scope.isdigit():
            new_msg_txt = msg_txt[:index] + repl_str + msg_txt[index:]
            return await self._insert_edit_msg(new_msg_txt, [match_str, repl_str, str(int(scope) - 1)], index + len(repl_str))
        else:
            return msg_txt

    @msg.command(name='edit', description='Edits the specified message')
    async def edit_msg(self, ctx, msg_id: int, *, words: str):
        """
        <words> must have the format command<delim>match<delim>replacement[<delim>count]

        where:
        <command> is:
            s - substitutes <replacement> based on the regex in <match>
            r - substitutes <match> with <replacement>
            i - inserts <replacement> after <match>

        <delim> can be any character that does not appear in <match>, <replacement> or <count>

        <count> is the max number of operations, do not specify count or use 'g' to have no limit

        <match> is the literal text to match or a python regular expression that matches the text to be replaced

        <replacement> is the literal text to be inserted
        """
        # for editing a message, expects a messge id
        # words is structured similar to a simple sed command
        if len(words) < 5:
            await ctx.channel.send('%s: edit_msg command expects the edit text to have form ```command/regex/replacement```' % ctx.author.mention)
            return
        cmd = words[0]
        delim = words[1]
        split_str = words[2:].split(delim)
        if len(split_str) == 2:
            split_str.append('g')
        # regex_str, repl_str = words[2:].split(delim)
        msg = None
        for guild in self.bot.guilds:
            for channel in guild.channels:
                try:
                    msg = await channel.fetch_message(msg_id)
                    if not isinstance(discord.Message, msg):
                        msg = None
                    break
                except:
                    pass

        if msg is None:
            await ctx.channel.send('%s: message id %s does not exist' % (ctx.author.mention, str(msg_id)))
            return
        msg_txt = msg.content
        msg_txt_final = await {'s': self._regex_sub_edit_msg, 'r': self._replace_edit_msg, 'i': self._insert_edit_msg}[cmd](msg_txt, split_str)
        await msg.edit(content=msg_txt_final)
        await ctx.channel.send('%s: message edited' % ctx.author.mention)


    @msg.command(name='edit_dry_run', description='Dry run of an edit command, prints the output to current channel in code format')
    async def edit_msg_dry_run(self, ctx, msg_id: int, *, words: str):
        """
        <words> must have the format command<delim>match<delim>replacement[<delim>count]

        where:
        <command> is:
            s - substitutes <replacement> based on the regex in <match>
            r - substitutes <match> with <replacement>
            i - inserts <replacement> after <match>

        <delim> can be any character that does not appear in <match>, <replacement> or <count>

        <count> is the max number of operations, do not specify count or use 'g' to have no limit

        <match> is the literal text to match or a python regular expression that matches the text to be replaced

        <replacement> is the literal text to be inserted
        """
        # for editing a message, expects a messge id
        # words is structured similar to a simple sed command
        if len(words) < 5:
            await ctx.channel.send('%s: edit_msg command expects the edit text to have form ```command/regex/replacement```' % ctx.author.mention)
            return
        cmd = words[0]
        delim = words[1]
        split_str = words[2:].split(delim)
        if len(split_str) == 2:
            split_str.append('g')
        # regex_str, repl_str = words[2:].split(delim)
        msg = None
        for guild in self.bot.guilds:
            for channel in guild.channels:
                try:
                    msg = await channel.fetch_message(msg_id)
                    if not isinstance(discord.Message, msg):
                        msg = None
                    break
                except Exception as e:
                    print(e)

        if msg is None:
            await ctx.channel.send('%s: message id %s does not exist' % (ctx.author.mention, str(msg_id)))
            return
        msg_txt = msg.content
        msg_txt_final = await {'s': self._regex_sub_edit_msg, 'r': self._replace_edit_msg, 'i': self._insert_edit_msg}[cmd](msg_txt, split_str)
        await ctx.channel.send('```\n%s\n```' % msg_txt_final)

    @msg.command(name='delete', description='Deletes the specified message')
    async def delete_msg(self, ctx, msg_id: int):
        msg = await ctx.fetch_message(msg_id)
        if msg is None:
            await ctx.channel.send('%s: message id %s does not exist' % (ctx.author.mention, str(msg_id)))
            return
        await msg.delete()
        await ctx.channel.send('%s: message deleted' % ctx.author.mention)


    @msg.command(name='replace', description='Replace the messge text with the text at a pastebin url')
    async def replace_msg(self, ctx, msg_id: int, url: str):
        msg = await ctx.fetch_message(msg_id)
        if msg is None:
            await ctx.channel.send('%s: message id %s does not exist' % (ctx.author.mention, str(msg_id)))
            return
        # url should point to a pastebin
        url = 'https://pastebin.com/raw/' + url[url.rfind('/') + 1:]  # paste key is everything past the last '/'
        r = requests.get(url)
        if r.status_code != 200 or r.text == 'Error with this ID!':
            await ctx.channel.send('Couldn\'t access url at ' + url + ' got return code: ' + str(r.status_code))
        else:
            await msg.edit(content=r.text)
            await ctx.channel.send('%s: message replaced' % ctx.author.mention)


    @commands.command(description='Posts a mentions a role in a channel')
    @commands.check(check_mod_perms)
    async def mention_role(self, ctx, channel: discord.TextChannel, role: discord.Role, *, msg: str=''):
        """
        role must be in quotes if the role has one or more spaces in it
        """
        if channel not in ctx.guild.channels:
            await ctx.channel.send("%s: That channel doesn't exist" % ctx.author.mention)
        elif role not in ctx.guild.roles:
            await ctx.channel.send("%s: That role doesn't exist" % ctx.author.mention)
        elif role.mentionable:
            await channel.send(role.mention)
        else:
            # role not mentionable
            await role.edit(mentionable=True)
            await channel.send(role.mention + ' ' + msg)
            await role.edit(mentionable=False)


def setup(bot):
    bot.add_cog(Mod(bot))
