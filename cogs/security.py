import discord
import re
import sqlite3
import asyncio

from config import loadconfig
from config import saveconfig

from icecream import ic
from discord.ext import commands
from datetime import datetime

ic.disable()


# Requested features for now:
#   Delete quick message sending, default: 5 msgs/4 secs
#   Delete mass mentions, default: 5+ mentions
#   Delete many caps, default: >70% caps
#   Delete server invites
#   Delete spammy duplicated text, some number of messages that are the same, default: 4
#   Automute for repeat violations

# Other automod features:
#   Delete words on a blacklist
#   Delete many emojis, default: >= 5
#   Link cooldown
#   Autoban mass mentions, default: >= 10
#   Delete embeds from self bots (?)
#   Delete all links
#   Attachment/embed cooldown
#   Warn for some offenses

# Other features:
#   Immune roles (eg mods)
#   Ignored channels
#   Link blacklist
#   Log channel
#   Configurable through group commands

# Implimentation details
#   Need to check which features are enabled and run through all of them in an on_message listener
#   Needs to be able to short circuit
#   Use a list of function names and iterate through it calling each one in turn
#     Functions will return one of several values what action to take, if any
#     Functions will have a decorator that checks if they are enabled
#       The decorator will return an empty function if feature is disabled
#     Functions will have a decorator to check if they are globally or locally exempt


class Security(commands.Cog):
    """commands for mod use only"""
    def __init__(self, bot):
        self.bot = bot
        self.conf = loadconfig('config.json')
        self.bot.add_listener(self.on_message_listener, 'on_message')
        self.settings = self.conf.get('security', {})
        self.log_channel = self.settings_load('action_log', default=None)
        for chnl in self.bot.guilds[0].text_channels:
            if chnl.name == self.log_channel:
                self.log_channel = chnl
                break
        self.ignored_channels = self.settings_load('ignored_channels', default=[])
        self.exempt_roles = self.settings_load('exempt_roles', default=[])
        self.exempt_users = self.settings_load('exempt_users', default=[])
        self.link_regex = re.compile(r"http[s]?://discord.gg/[a-zA-Z0-9]+")
        self.prank_users = []
        self.db = None

        try:
            self.db = sqlite3.connect(self.conf['bumpsDB_path'])
            with self.db:
                c = self.db.cursor()
                c.execute('SELECT SQLITE_VERSION()')
                data = c.fetchone()
                self.sqlite_version = data[0]
        except Exception as e:
            raise e

        self.IGNORE = 0
        self.MUTE = 1
        self.DELETE = 2
        self.BAN = 3
        self.VIOLATION = 4
        self.TEMPMUTE = 5

        self.sec_feature_names = ['quick_message', 'mass_mention_mute', 'many_caps_delete', 'delete_invites', 'delete_spam', 'delete_repeated_chars', 'word_blacklist', 'delete_many_emojis', 'link_cooldown',
                                  'mass_mention_ban', 'delete_all_links', 'attachment_cooldown', 'automute']
        print('| Loaded:   security')


    def check_perms(ctx):
        return ctx.author.guild_permissions.kick_members


    def activated(self, setting_name):
        return self.settings_load(setting_name, 'activated', default=False)


    def settings_load(self, *args, default={}):
        temp = self.settings
        for i in range(len(args) - 1):
            temp = temp.get(args[i], {})
        return temp.get(args[-1], default)


    def reload(self):
        self.conf = loadconfig('config.json')
        self.settings = self.conf.get('security', {})
        self.ignored_channels = self.settings_load('ignored_channels', default=[])
        self.exempt_roles = self.settings_load('exempt_roles', default=[])
        self.exempt_users = self.settings_load('exempt_users', default=[])


    async def log(self, action_string):
        await self.log_channel.send(action_string)


    def is_exempt(self, message):
        if isinstance(message.channel, discord.DMChannel) or message.channel.name in self.ignored_channels:
            return True
        g = self.bot.guilds[0]
        mem = g.get_member(int(message.author.id))
        author_roles = mem.roles
        for r in author_roles:
            if r.name in self.exempt_roles:
                return True
        if message.author.id in self.exempt_users:
            return True
        return False


    async def _prank(self, message):
        if message.author.id not in self.prank_users:
            return

        def check(localmessage):
            return localmessage.author == message.author

        try:
            msg = await self.bot.wait_for('message_delete', timeout=15.0, check=check)
            await msg.channel.send("In case you missed what {} deleted:\n\n{}".format(msg.author.name, msg.content))
        except asyncio.TimeoutError:
            pass


    # command & control for automod features
    @commands.group(pass_context=True)
    @commands.check(check_perms)
    async def prank(self, ctx):
        if ctx.invoked_subcommand is None:
            pass


    @prank.command(name='add', description="add user to prank list")
    async def _prank_add(self, ctx, user: discord.Member):
        if user.id not in self.prank_users:
            self.prank_users.append(user.id)
            await ctx.channel.send("{}: user added".format(ctx.author.mention))
        else:
            await ctx.channel.send("{}: user already on list".format(ctx.author.mention))


    @prank.command(name='remove', description="remove a user from the prank list")
    async def _prank_remove(self, ctx, user: discord.Member):
        if user.id in self.prank_users:
            self.prank_users.remove(user.id)
            await ctx.channel.send("{}: user removed".format(ctx.author.mention))
        else:
            await ctx.channel.send("{}: user not in list".format(ctx.author.mention))


    @prank.command(name='list', description="list users on the prank list")
    async def _prank_list(self, ctx):
        bld_str = '```\n'
        for uid in self.prank_users:
            bld_str += str(uid) + '\n'
        bld_str += '```\n'
        await ctx.channel.send("{}:\n{}".format(ctx.author.mention, bld_str))



    # need to add checks for exemptions
    async def on_message_listener(self, message):
        await self._prank(message)
        if not self.is_exempt(message):
            for feature in self.sec_feature_names:
                if not self.activated(feature):
                    continue
                result = await getattr(self, feature)(message)
                if result is not None:
                    user_action = result.get('user', self.IGNORE)
                    message_action = result.get('message', self.IGNORE)
                    ic(user_action, message_action)
                    await {
                        self.IGNORE: self.ignore,
                        self.VIOLATION: self.automute,
                        self.MUTE: self.mute_user,
                        self.TEMPMUTE: self.temp_mute,
                        self.BAN: self.ban_user
                    }.get(user_action)(message)
                    await {
                        self.IGNORE: self.ignore,
                        self.DELETE: self.delete_message
                    }.get(message_action)(message)


    # actions to take
    # TODO: logging automod actions
    async def mute_user(self, message):
        # 'mutes' aren't really a thing, implimentations are to assign a role that doesn't have any perms
        mute_role = self.settings_load('mute_role', default='')
        for r in message.guild.roles:
            if mute_role == r.name and r not in message.author.roles:
                await message.author.add_roles(*[r])
                break


    async def temp_mute(self, message):
        """
        Usage: mod tempmute user timespan_in_hours [reason]
        timespan is in hours
        """
        user_id = message.author.id
        server_id = message.guild.id
        tempmutes = loadconfig('tempmutes.json')
        if tempmutes.get(str(user_id) + '_' + str(server_id), {}).get('endTime', None) is None:
            await self.mute_user(message)
            tempmutes[str(user_id) + '_' + str(server_id)] = {
                'endTime': datetime.now().timestamp() + (self.settings.get('temp_mute_time', 24) * 60 * 60),
                'reason': 'temp mute'
            }
            saveconfig(tempmutes, 'tempmutes.json')


    async def ban_user(self, message):
        await message.author.ban()


    async def ignore(*args, **kwargs):
        # essentially a blackhole
        return None


    async def automute(self, message):
        if self.settings_load('automute', 'activated', default=False) is False:
            return
        limit = self.settings_load('automute', 'limit', default=4)
        if limit < 0:
            return None
        curr_violations = await self.violation(message)
        if curr_violations >= limit:
            await self.mute_user(message)
            await self.log("Muting %s for too many rule violations" % ("name: " + message.author.display_name + " ID: " + str(message.author.id)))



    async def violation(self, message):
        user_id = message.author.id
        server_id = message.guild.id
        violations = loadconfig('violations.json')
        count = violations.get(str(user_id) + '_' + str(server_id), None)
        if count is None:
            # they don't have an entry yet, create one
            violations[str(user_id) + '_' + str(server_id)] = 1
            saveconfig(violations, 'violations.json')
            return 1
        else:
            violations[str(user_id) + '_' + str(server_id)] += 1
            saveconfig(violations, 'violations.json')
            return count + 1



    async def delete_message(self, message):
        ic(message.content)
        await message.delete()


    # actual checks to perform on messages
    async def quick_message(self, message):
        # check the number of messages sent in a given channel by a single user for a certain amount of time is under a certain number
        limit = self.settings_load('quick_message', 'limit', default=5)
        timespan = self.settings_load('quick_message', 'timespan', default=4)  # time in seconds
        msgs = await message.channel.history(limit=2 * limit).flatten()
        count = 0
        for m in msgs:
            if m.author == message.author and (message.created_at - m.created_at).seconds <= timespan:
                count += 1
            if count >= limit:
                await self.log("Muting ```%s``` for sending too many messages too quickly" % ("name: " + message.author.display_name + " ID: " + str(message.author.id)))
                await self.violation(message)
                return {'user': self.TEMPMUTE, 'message': self.DELETE}
        return None



    async def mass_mention_mute(self, message):
        limit = self.settings_load('mass_mention_mute', 'limit', default=5)
        mention_num = len(message.mentions)
        if mention_num >= limit:
            await self.violation(message)
            return {'user': self.MUTE, 'message': self.DELETE}
        else:
            None


    async def many_caps_delete(self, message):
        max_perc = self.settings_load('many_caps_delete', 'percentage', default=70)
        tot = len(message.content)
        caps = sum([1 for c in message.content if c.isupper()])
        perc_caps = (caps / tot) * 100
        if perc_caps >= max_perc:
            return {'user': self.VIOLATION, 'message': self.DELETE}
        else:
            return None


    async def delete_invites(self, message):
        search_result = self.link_regex.search(message.content)
        ic(search_result)
        if search_result is not None and search_result not in self.settings_load('delete_invites', 'whitelist', default=[]):
            return {'user': self.VIOLATION, 'message': self.DELETE}
        else:
            return None


    async def delete_spam(self, message):
        limit = self.settings_load('delete_spam', 'repeated', default=4)
        msgs = await message.channel.history(limit=2 * limit).flatten()
        count = 0
        deletes = []
        for msg in msgs:
            if msg.content == message.content:
                deletes.append(msg)
                count += 1
                if count >= limit:
                    # would like to delete higher up, but /shrug
                    for i in range(len(deletes) - 1, 0, -1):
                        await deletes[i].delete()
                    await self.violation(message)
                    return {'user': self.MUTE, 'message': self.DELETE}
        return None


    async def delete_repeated_chars(self, message):
        limit = self.settings_load('delete_repeated_chars', 'limit', default=10)
        chunk_size = self.settings_load('delete_repeated_chars', 'chunk_size', default=1)
        content = message.content
        cont_len = len(content)
        if cont_len < limit * chunk_size:  # if the message is to short to cause a violation, don't bother
            return None
        curr_count = 0
        curr_char = content[:chunk_size]
        for i in range(chunk_size, len(content), chunk_size):
            if content[i - chunk_size:i] == curr_char:
                curr_count += 1
            else:
                curr_count = 1
            if curr_count == limit:
                return {'user': self.VIOLATION, 'message': self.DELETE}
            elif cont_len - (chunk_size * (limit - curr_count - 1)) < i:  # if the remaining message is too short to cause a violation, quit
                return None



    async def word_blacklist(self, message):
        blacklist = self.settings_load('word_blacklist', 'blacklist', default=[])
        for w in blacklist:
            if w in message.content:
                return {'user': self.VIOLATION, 'message': self.DELETE}


    async def delete_many_emojis(self, message):
        return None


    async def link_cooldown(self, message):
        return None


    async def mass_mention_ban(self, message):
        limit = self.settings_load('mass_mention_ban', 'limit', default=10)
        mention_num = len(message.mentions)
        if mention_num >= limit:
            return {'user': self.BAN, 'message': self.DELETE}
        else:
            None


    async def delete_all_links(self, message):
        return None


    async def attachment_cooldown(self, message):
        return None



    # command & control for automod features
    @commands.group(pass_context=True)
    @commands.check(check_perms)
    async def sec(self, ctx):
        """group command testing"""
        if ctx.invoked_subcommand is None:
            pass


    @sec.command(name='add')
    async def _add(self, ctx, a: int, b: int):
        await ctx.channel.send(a + b)


    @sec.command(name='reload')
    async def _reload(self, ctx):
        self.reload()
        await ctx.channel.send("%s: reloaded security config" % ctx.author.mention)


    @sec.command(name='list')
    async def _list(self, ctx):
        lines = []
        longest = 0
        for f in self.sec_feature_names:
            if longest < len(f):
                longest = len(f)
        for f in self.sec_feature_names:
            lines.append(f + (' ' * (longest - len(f))) + ": " + str(self.activated(f)))
        bld_str = '```\n' + '\n'.join(lines) + '\n```'
        await ctx.channel.send("%s:\n%s" % (ctx.author.mention, bld_str))


    @sec.command(name='activate')
    async def _activate(self, ctx, name: str):
        if name not in self.sec_feature_names:
            await ctx.channel.send('%s: "%s" is not a known security feature, make sure you have spelled it correctly' % (ctx.author.mention, name))
        else:
            self.reload()
            self.conf['security'][name]['activated'] = True
            saveconfig(self.conf)
            await ctx.channel.send("%s: %s activated" % (ctx.author.mention, name))


    @sec.command(name='deactivate')
    async def _deactivate(self, ctx, name: str):
        if name not in self.sec_feature_names:
            await ctx.channel.send('%s: "%s" is not a known security feature, make sure you have spelled it correctly' % (ctx.author.mention, name))
        else:
            self.reload()
            self.conf['security'][name]['activated'] = False
            saveconfig(self.conf)
            await ctx.channel.send("%s: %s deactivated" % (ctx.author.mention, name))


    @sec.command(name='option_list')
    async def _opt_list(self, ctx, name: str):
        if name not in self.sec_feature_names:
            await ctx.channel.send('%s: "%s" is not a known security feature, make sure you have spelled it correctly' % (ctx.author.mention, name))
        else:
            lines = []
            longest = 0
            defines = self.settings_load(name)
            for s in defines:
                if longest < len(s):
                    longest = len(s)
            for s in defines:
                lines.append(s + (' ' * (longest - len(s))) + ': ' + str(self.settings_load(name, s, default='undefined')))
                bld_str = '```\n' + '\n'.join(lines) + '\n```'
            await ctx.channel.send("%s:\n%s" % (ctx.author.mention, bld_str))


    @sec.command(name='option_set')
    async def _opt_set(self, ctx, name: str, option: str, *, new_val: str):
        if name not in self.sec_feature_names:
            await ctx.channel.send('%s: "%s" is not a known security feature, make sure you have spelled it correctly' % (ctx.author.mention, name))
        elif self.settings_load(name, option, default=None) is None:
            await ctx.channel.send('%s: "%s" is not a known option for %s, make sure you have spelled it correctly' % (ctx.author.mention, option, name))
        else:
            self.reload()
            val = new_val
            if new_val == 'True' or new_val == 'true':
                val = True
            elif new_val == 'False' or new_val == 'false':
                val = False
            elif new_val.isdigit():
                val = int(new_val)
            elif len(new_val) >= 2 and new_val.startswith('-') and new_val[1:].isdigit():
                val = int(new_val)
            self.conf['security'][name][option] = val
            saveconfig(self.conf)
            await ctx.channel.send("%s: %s set as %s" % (ctx.author.mention, option, val))


    @sec.command(name='option_list_edit')
    async def _opt_list_edit(self, ctx, name: str, option: str, cmd: str, interp: str, *, new_val: str):
        # name and option as above
        # cmd is one of [set, add, remove]
        # interp is one of [string, int, bool]
        # new_val is comma seperated list of values
        cmd = cmd.lower()
        interp = interp.lower()
        if name not in self.sec_feature_names:
            await ctx.channel.send('%s: "%s" is not a known security feature, make sure you have spelled it correctly' % (ctx.author.mention, name))
        elif self.settings_load(name, option, default=None) is None:
            await ctx.channel.send('%s: "%s" is not a known option for %s, make sure you have spelled it correctly' % (ctx.author.mention, option, name))
        elif cmd not in ['set', 'add', 'remove']:
            await ctx.channel.send('%s: cmd must be one of ["set", "add", "remove"], not %s' % (ctx.author.mention, cmd))
        elif interp not in ['string', 'int', 'bool']:
            await ctx.channel.send('%s: interp must be one of ["string", "int", "bool"], not %s' % (ctx.author.mention, cmd))
        else:
            val = [{'string': str, 'int': int, 'bool': bool}.get(interp)(x.strip()) for x in new_val.split(',')]
            curr = self.settings_load(name, option, default=[])
            if cmd == 'add':
                for v in curr:
                    if v not in val:
                        val.append(v)
            elif cmd == 'remove':
                final = []
                for v in curr:
                    if v not in val:
                        final.append(v)
                val = final
            self.reload()
            self.conf['security'][name][option] = val
            saveconfig(self.conf)
            await ctx.channel.send("%s: %s set as %s" % (ctx.author.mention, option, val))


    @sec.command(name='get_vio', description='get the number of violations for a user')
    async def _get_violation_count(self, ctx, member: discord.Member):
        uid_sid = str(member.id) + '_' + str(ctx.guild.id)
        violations = loadconfig('violations.json')

        count = violations.get(uid_sid, None)
        if count is None:
            # they don't have an entry yet
            await ctx.channel.send("%s: that member has 0 outstanding violations" % ctx.message.author.mention)
        else:
            await ctx.channel.send("%s: that member has %s outstanding violations" % (ctx.message.author.mention, str(count)))


    @sec.command(name='set_vio', description='get the number of violations for a user')
    async def _set_violation_count(self, ctx, member: discord.Member, new_count: int):
        uid_sid = str(member.id) + '_' + str(ctx.guild.id)
        violations = loadconfig('violations.json')
        violations[uid_sid] = new_count
        saveconfig(violations, 'violations.json')
        await ctx.channel.send("%s: that member now has %s outstanding violations" % (ctx.message.author.mention, str(new_count)))


def setup(bot):
    bot.add_cog(Security(bot))
