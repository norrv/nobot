import discord
import json
import requests
import re

from config import loadconfig
from config import saveconfig

from icecream import ic
from discord.ext import commands
from datetime import datetime
from datetime import timedelta

ic.disable()


class Alias(commands.Cog):
    """commands for mod use only"""
    def __init__(self, bot):
        self.bot = bot
        self.bot.add_listener(self.on_message_listener, 'on_message')
        self.conf = loadconfig('config.json')
        self.aliases = self.conf['aliases']  # is a dict
        print('| Loaded:   alias')


    def check_perms(ctx):
        return ctx.author.guild_permissions.administrator


    async def on_message_listener(self, message):
        # test stuff
        # print(int(msgs[-1].created_at - message.created_at) <= 5)
        # check message for aliases
        if not message.author.bot and len(message.content) >= 2 and message.content[0] == self.bot.command_prefix:
            ic(message.content)
            alias = None
            command = message.content.split()[0][1:]
            for k in self.aliases.keys():
                if command == k:
                    alias = k
                    break
            if alias is not None:
                message.content = self.bot.command_prefix + self.aliases[alias] + message.content[len(self.bot.command_prefix) + len(alias):]
            print("%s | user '%s' running '%s'" % (datetime.now().strftime("%Y-%m-%d %H:%M:%S"), message.author.name, message.content))
            await self.bot.process_commands(message)



    @commands.group(pass_context=True)
    async def alias(self, ctx):
        """custom program commands group"""
        if ctx.invoked_subcommand is None:
            pass


    @alias.command(name='add', description='add an alias')
    @commands.check(check_perms)
    async def add_alias(self, ctx, alias: str, *, repl: str):
        """
        The alias may only be a single word.
        However, an alias may map to multiple words.
        For example, "aa" may map to "alias add"
        but "a a" cannot
        """
        self.aliases[alias] = repl
        self.conf = loadconfig()
        self.conf['aliases'][alias] = repl
        saveconfig(self.conf)
        await ctx.channel.send("%s: added %s as an alias for %s" % (ctx.author.mention, alias, repl))


    @alias.command(name='remove', description='remove an alias')
    @commands.check(check_perms)
    async def remove_alias(self, ctx, alias: str):
        if self.aliases.pop(alias, None) is None:
            await ctx.channel.send("%s: %s is not an alias" % (ctx.author.mention, alias))
        else:
            self.conf = loadconfig()
            self.conf['aliases'].pop(alias, None)
            saveconfig(self.conf)
            await ctx.channel.send("%s: alias %s removed" % (ctx.author.mention, alias))


    @alias.command(name='list', description='list current aliases')
    async def list_aliases(self, ctx):
        bld_str = '%s: current aliases:\n```\n' % ctx.author.mention
        astrs = []
        for k in self.aliases.keys():
            astrs.append(k + "=" + self.aliases[k])
        bld_str += '\n'.join(astrs) + '\n```'
        await ctx.channel.send(bld_str)


    @alias.command(name='show', description='remove an alias')
    async def show_alias(self, ctx, alias: str):
        if self.aliases.pop(alias, None) is None:
            await ctx.channel.send("%s: %s is not an alias" % (ctx.author.mention, alias))
        else:
            bld_str = "%s:\n```\n" % ctx.author.mention
            bld_str += self.aliases[alias] + "\n```"
            await ctx.channel.send(bld_str)


def setup(bot):
    bot.add_cog(Alias(bot))
