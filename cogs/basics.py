import discord
import sqlite3
import asyncio
import random
#import hashlib

from config import loadconfig
from discord.ext import commands
from random import randint
#from nltk.corpus import cmudict
#
#word_dict = cmudict.dict()
#
#def syllables(word):
    ##referred from stackoverflow.com/questions/14541303/count-the-number-of-syllables-in-a-word
    #count = 0
    #vowels = 'aeiouy'
    #word = word.lower()
    #if word[0] in vowels:
        #count += 1
    #for index in range(1,len(word)):
        #if word[index] in vowels and word[index-1] not in vowels:
            #count += 1
    #if word.endswith('e'):
        #count -= 1
    #if word.endswith('le'):
        #count += 1
    #if count == 0:
        #count += 1
    #return count
#
#def nsyl(word):
    #try:
        #return [len(list(y for y in x if y[-1].isdigit())) for x in word_dict[word.lower()]][0]
    #except KeyError:
        ##if word not found in cmudict
        #return syllables(word)
#
#def is_haiku(poem):
    ## poem should be an array with three lines
    #if len(poem) != 3:
        #return False
    #syllables = []
    #for line in poem:
        #line_count = 0
        #for w in line.split():
            #w = w.strip().strip('.,!@#$%^&*()"\'')
            #if len(w) == 0:
                #continue
            #line_count += nsyl(w)
        #syllables.append(line_count)
    #if syllables[0] == 5 and syllables[1] == 7 and syllables[2] == 5:
        #return True
    #else:
        #return False
#
#def clean_poem(poem):
    #poem = list(filter(lambda l: len(l) > 0, [s.strip() for s in poem.split('\n')]))
    #lines = []
    #for line in poem:
        #this_line = []
        #for w in line.split():
            #w = w.strip().strip('.,!@#$%^&*()"\'')
            #if len(w) == 0:
                #continue
            #this_line.append(w)
        #lines.append(' '.join(this_line))
    #return lines


class Basics(commands.Cog):
    """some basic commands exposed to the user"""
    def __init__(self, bot):
        self.bot = bot
        self.conf = loadconfig()
        self.echo_blacklist = []  # strings
        self.echo_channel_blacklist = ["new_arrivals", "air_temple"]
        self.echo_user_blacklist = []  # ints
        print('| Loaded:   basics')


    def check_echo_perms(ctx):
        return ctx.author.guild_permissions.administrator


    @commands.command(description='Repeat whatever is passed')
    @commands.check(check_echo_perms)
    async def echo(self, ctx, *, words: str):
        for w in self.echo_blacklist:
            if w in words:
                return
        for uid in self.echo_user_blacklist:
            if ctx.author.id == uid:
                return
        for cname in self.echo_channel_blacklist:
            if ctx.channel.name == cname:
                return
        pmsg = await ctx.message.channel.send("from {}: ".format(ctx.author.name) + '``' + words + '``')

        def check(message):
            return message.author == ctx.author

        try:
            msg = await self.bot.wait_for('message_delete', timeout=60.0, check=check)
            await pmsg.delete()
            print("deleting {}'s echo'ed message: {}".format(ctx.author.name, words))
        except asyncio.TimeoutError:
            print("not deleting {}'s echo'ed message: {}".format(ctx.author.name, words))
        else:
            print("huh")


    @commands.command(description='Test if the bot is still alive')
    @commands.check(check_echo_perms)
    async def ping(self, ctx):
        try:
            await ctx.message.channel.send('Pong!')
        except Exception as e:
            print('ERROR: ping:', e)


    @commands.command(description='yip yip')
    async def yipyip(self, ctx):
        try:
            if ctx.message.channel.id in [372086844956868618, 721604232532459540, 372087003669331969, 498253602788343827, 709827097559826553]:
                await ctx.message.delete()
                return
            await ctx.message.channel.send('RARGH!')
        except Exception as e:
            print('ERROR: ping:', e)


    #@commands.command(description='*laser noises*')
    #async def BWUB(self, ctx):
        #try:
            #gifs = [
                #'https://cdn.discordapp.com/attachments/423624377318506507/720425755049459731/image0.gif',
                #'https://cdn.discordapp.com/attachments/423624377318506507/720425786515128430/image1.gif',
                #'https://cdn.discordapp.com/attachments/423624377318506507/720425803224973322/image2.gif'
            #]
            #index = random.randint(0, len(gifs) -1)
            #await ctx.message.channel.send(gifs[index])
        #except Exception as e:
            #print('ERROR: ping:', e)


    @commands.command(description='The best fanfic')
    async def sub(self, ctx):
        try:
            suburl = 'https://archiveofourown.org/works/9115681/chapters/20718460'
            await ctx.message.channel.send('You should read the amazingly well written and interesting Legend of Korra fanfiction Subduction!\n{}'.format(suburl))
        except Exception as e:
            print('ERROR: sub:', e)


    # @commands.command(description='ban a fool')
    # async def ban(self, ctx, mem: discord.Member):
        # name = mem.display_name
        # is_everyone = 'everyone' in name
        # is_here = 'here' in name
        # name = name.replace('@', 'at')
        # if ctx.author.id == '338730322843926530' or mem.id == '424659040170409984' or randint(0, 100) < 10 or is_everyone or is_here:
            # await ctx.channel.send('ban hammer dropped on ' + ctx.author.mention)
        # else:
            # await ctx.channel.send('ban hammer dropped on ' + name)


    @commands.command(description='Get a single member\'s id')
    async def ids(self, ctx, *, user: discord.Member):
        await ctx.message.channel.send(user.id)


    @commands.command(description='Get a member\'s number of bumps')
    async def bumps(self, ctx, *, user: discord.Member=None):
        mem = ctx.author if user is None else user
        try:
            db = sqlite3.connect(self.conf['bumpsDB_path'])
            with db:
                c = db.cursor()
                c.execute("SELECT bumps FROM bumpers WHERE userId=? AND serverId=?", (mem.id, ctx.guild.id))
                row = c.fetchone()

                if row is None:
                    if mem == user:
                        await ctx.channel.send('%s: %s never bumped this server. :(' % (ctx.author.mention, mem.name))
                    else:
                        await ctx.channel.send('%s: You never bumped this server. :(' % ctx.author.mention)
                else:
                    if mem == user:
                        await ctx.channel.send('%s: %s has bumped this server %s times!' % (ctx.author.mention, mem.name, str(row[0])))
                    else:
                        await ctx.channel.send('%s: You have bumped this server %s times!' % (ctx.author.mention, str(row[0])))
        except Exception as e:
            raise e


    #@commands.command(description='Can you hack it in the Five-Seven-Five society?')
    #@commands.check(check_echo_perms)
    #async def haiku(self, ctx, *, poem: str=''):
        #"""
        #Usage haiku [poem]
#
        #make sure you put each line of the poem on a new line
        #"""
        #poem = clean_poem(poem)
        #if is_haiku(poem):
            ## check if we've already got this haiku
            #hashes = []
            #with open('haikus.txt', 'r') as f:
                #hashes = [l.strip() for l in f.readlines()]
            #poem_hash = hashlib.sha256('\n'.join(poem).encode('utf-8')).hexdigest()
            #if poem_hash in hashes:
                ## tell them someone already submitted that haiku
                #await ctx.channel.send('Try to come up with something more original')
                #return
            #hashes.append(poem_hash)
            #with open('haikus.txt', 'w') as f:
                #f.write('\n'.join(hashes))
#
            ## if not, give them the role
            #for r in ctx.guild.roles:
                #if r.id == 724751859356794880:
                    #await ctx.author.add_roles(r)
            #await ctx.channel.send('What a Remarkable Oaf!')
        #else:
            #await ctx.channel.send('That\'s not a haiku\n(you might need to fix spelling or punctuation)')



def setup(bot):
    bot.add_cog(Basics(bot))
