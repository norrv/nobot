import internals
from discord.ext import commands
class Atla:
	"""Compiled for custom command atla"""
	def __init__(self, bot):
		self.bot = bot
		print('| Loaded:   atla')
	@commands.command(description='atla custom command')
	async def atla(self, ctx, *, args: str=""):
		await internals.role(ctx, '+ATLA')
		await ctx.channel.send("{} has joined the ATLA stream!".format(await internals.user(ctx)))
def setup(bot):
	bot.add_cog(Atla(bot))