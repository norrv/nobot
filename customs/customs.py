import internals
from discord.ext import commands
class Customs(commands.Cog):
    """Only shows the commands you can currently run based on your roles"""
    def __init__(self, bot):
        self.bot = bot
        print('| Loaded:   Customs')

    #START ATLA

    @commands.command(description='ATLA custom command')
    async def atla(self, ctx, *, args: str=""):
        await internals.role(ctx, '+ATLA')
        await ctx.channel.send("{} has joined the ATLA stream!".format(await internals.user(ctx)))

    #STOP ATLA
    #START movies

    @commands.command(description='movies custom command')
    async def movies(self, ctx, *, args: str=""):
        await internals.role(ctx, 'Movies')
        await ctx.channel.send("{} has joined the movie stream!".format(await internals.user(ctx)))

    #STOP movies
    #START reddit

    @commands.command(description='reddit custom command')
    async def reddit(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Redditor')
        await ctx.channel.send("{} has become a Redditor!".format(await internals.user(ctx)))

    #STOP reddit
    #START removeatla

    @commands.command(description='removeatla custom command')
    async def removeatla(self, ctx, *, args: str=""):
        await internals.role(ctx, '-ATLA')
        await ctx.channel.send("{} is no longer part of the ATLA stream!".format(await internals.user(ctx)))

    #STOP removeatla
    #START removeavatarmc

    @commands.command(description='removeavatarmc custom command')
    async def removeavatarmc(self, ctx, *, args: str=""):
        await internals.role(ctx, '-AvatarMC')
        await ctx.channel.send("{} is no longer a minecrafter!".format(await internals.user(ctx)))

    #STOP removeavatarmc
    #START removemovies

    @commands.command(description='removemovies custom command')
    async def removemovies(self, ctx, *, args: str=""):
        await internals.role(ctx, '-Movies')
        await ctx.channel.send("{} is no longer part of the movie stream!".format(await internals.user(ctx)))

    #STOP removemovies
    #START removereddit

    @commands.command(description='removereddit custom command')
    async def removereddit(self, ctx, *, args: str=""):
        await internals.role(ctx, '-Redditor')
        await ctx.channel.send("{} is no longer a Redditor!".format(await internals.user(ctx)))

    #STOP removereddit
    #START removerp

    @commands.command(description='removerp custom command')
    async def removerp(self, ctx, *, args: str=""):
        await internals.role(ctx, '-Roleplayer')
        await ctx.channel.send("{} is no longer a roleplayer!".format(await internals.user(ctx)))

    #STOP removerp
    #START weightless

    @commands.command(description='weightless custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Airbender')
    async def weightless(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Weightless', '-Projection', '-Breathbender')
        await ctx.channel.send("{} has let go of their earthly tethers!".format(await internals.user(ctx)))

    #STOP weightless














    #START projection

    @commands.command(description='projection custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Airbender')
    async def projection(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Projection', '-Weightless', '-Breathbender')
        await ctx.channel.send("{} has mastered the art of projection!".format(await internals.user(ctx)))

    #STOP projection

    #START sword

    @commands.command(description='sword custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Nonbender')
    async def sword(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Swordmaster', '-Chiblocker', '-Kyoshi Warrior')
        await ctx.channel.send("{} has become a swordmaster!".format(await internals.user(ctx)))

    #STOP sword

    #START kyoshi

    @commands.command(description='kyoshi custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Nonbender')
    async def kyoshi(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Kyoshi Warrior', '-Chiblocker', '-Swordmaster')
        await ctx.channel.send("{} has become a Kyoshi Warrior!".format(await internals.user(ctx)))

    #STOP kyoshi

    #START spirit

    @commands.command(description='spirit custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Waterbender')
    async def spirit(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Spiritbender', '-Healer', '-Bloodbender')
        await ctx.channel.send("{} has become a spiritbender!".format(await internals.user(ctx)))

    #STOP spirit









    #START healer

    @commands.command(description='healer custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Waterbender')
    async def healer(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Healer', '-Bloodbender', '-Spiritbender')
        await ctx.channel.send("{} has mastered the art of healing!".format(await internals.user(ctx)))

    #STOP healer


    #START blood

    @commands.command(description='blood custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Waterbender')
    async def blood(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Bloodbender', '-Healer', '-Spiritbender')
        await ctx.channel.send("{} has become a bloodbender!".format(await internals.user(ctx)))

    #STOP blood

    #START chiblocker

    @commands.command(description='chiblocker custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Nonbender')
    async def chiblocker(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Chiblocker', '-Swordmaster', '-Kyoshi Warrior')
        await ctx.channel.send("{} has become a chiblocker!".format(await internals.user(ctx)))

    #STOP chiblocker

    #START game

    @commands.command(description='game custom command')
    async def game(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Gaming')
        await ctx.channel.send("{} is now a Gamer!".format(await internals.user(ctx)))

    #STOP game

    #START removegame

    @commands.command(description='removegame custom command')
    async def removegame(self, ctx, *, args: str=""):
        await internals.role(ctx, '-Gaming')
        await ctx.channel.send("{} is no longer a Gamer!".format(await internals.user(ctx)))

    #STOP removegame

    #START events

    @commands.command(description='events custom command')
    async def events(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Events')
        await ctx.channel.send("{} will now be notified of events!".format(await internals.user(ctx)))

    #STOP events



    #START removeevents

    @commands.command(description='removeevents custom command')
    async def removeevents(self, ctx, *, args: str=""):
        await internals.role(ctx, '-Events')
        await ctx.channel.send("{} will no longer be notified of events!".format(await internals.user(ctx)))

    #STOP removeevents

    #START frame

    @commands.command(description='frame custom command')
    async def frame(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Frame Game')
        await ctx.channel.send("{} will now be notified of future Frame Games!".format(await internals.user(ctx)))

    #STOP frame

    #START removeframe

    @commands.command(description='removeframe custom command')
    async def removeframe(self, ctx, *, args: str=""):
        await internals.role(ctx, '-Frame Game')
        await ctx.channel.send("{} will no longer be notified of future Frame Games!".format(await internals.user(ctx)))

    #STOP removeframe


    #START removerpg

    @commands.command(description='removerpg custom command')
    async def removerpg(self, ctx, *, args: str=""):
        await internals.role(ctx, '-RPG')
        await ctx.channel.send("{} will no longer be notified about RPG events!".format(await internals.user(ctx)))

    #STOP removerpg

    #START lok

    @commands.command(description='lok custom command')
    async def lok(self, ctx, *, args: str=""):
        await internals.role(ctx, '+LoK')
        await ctx.channel.send("{} has joined the LoK stream!".format(await internals.user(ctx)))

    #STOP lok

    #START removelok

    @commands.command(description='removelok custom command')
    async def removelok(self, ctx, *, args: str=""):
        await internals.role(ctx, '-LoK')
        await ctx.channel.send("{} is no longer part of the LoK stream!".format(await internals.user(ctx)))

    #STOP removelok

    #START serious

    @commands.command(description='serious custom command')
    async def serious(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Serious')
        await ctx.channel.send("{} has joined the serious discussion channel, {}!".format(await internals.user(ctx), await internals.mention_channel(ctx, 'serious_topics')))

    #STOP serious

    #START removeserious

    @commands.command(description='removeserious custom command')
    async def removeserious(self, ctx, *, args: str=""):
        await internals.role(ctx, '-Serious')
        await ctx.channel.send("{} is no longer part of the serious discussion channel, {}!".format(await internals.user(ctx), await internals.mention_channel(ctx, 'serious_topics')))

    #STOP removeserious

    #START chireader

    @commands.command(description='chireader custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Firebender')
    async def chireader(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Chireader', '-Lightningbender', '-Combustionbender')
        await ctx.channel.send("has become a chi reader!".format())

    #STOP chireader

    #START lightning

    @commands.command(description='lightning custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Firebender')
    async def lightning(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Lightningbender', '-Combustionbender', '-Chireader')
        await ctx.channel.send("\r\n{} has become a lightningbender!".format(await internals.user(ctx)))

    #STOP lightning

    #START combustion

    @commands.command(description='combustion custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Firebender')
    async def combustion(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Combustionbender', '-Lightningbender', '-Chireader')
        await ctx.channel.send("\r\n{} has become a combustionbender!".format(await internals.user(ctx)))

    #STOP combustion

    #START Seismic Sense

    @commands.command(description='Seismic Sense custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Earthbender')
    async def seismicsense(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Seismic Sense', '-Sandbender', '-Lavabender', '-Metalbender')
        await ctx.channel.send("\r\n{} has perfected Seismic Sense!".format(await internals.user(ctx)))

    #STOP Seismic Sense

    #START sand

    @commands.command(description='sand custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Earthbender')
    async def sand(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Sandbender', '-Lavabender', '-Metalbender', '-Seismic Sense')
        await ctx.channel.send("\r\n{} has become a sandbender!".format(await internals.user(ctx)))

    #STOP sand

    #START metal

    @commands.command(description='metal custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Earthbender')
    async def metal(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Metalbender', '-Lavabender', '-Sandbender', '-Seismic Sense')
        await ctx.channel.send("\r\n{} has become a metalbender!".format(await internals.user(ctx)))

    #STOP metal

    #START lava

    @commands.command(description='lava custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Earthbender')
    async def lava(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Lavabender', '-Metalbender', '-Sandbender', '-Seismic Sense')
        await ctx.channel.send("\r\n{} has become a lavabender!".format(await internals.user(ctx)))

    #STOP lava

    #START none

    @commands.command(description='none custom command')
    async def none(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Nonbender', '-Earthbender', '-Waterbender', '-Firebender', '-Airbender', '-Lightningbender', '-Combustionbender', '-Healer', '-Bloodbender', '-Weightless', '-Breathbender', '-Projection', '-Lavabender', '-Metalbender', '-Sandbender', '-Spiritbender', '-Chireader', '-Seismic Sense')
        await ctx.channel.send("{} has chosen to be equalized!".format(await internals.user(ctx)))

    #STOP none

    #START water

    @commands.command(description='water custom command')
    async def water(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Waterbender', '-Earthbender', '-Firebender', '-Airbender', '-Nonbender', '-Lightningbender', '-Combustionbender', '-Weightless', '-Breathbender', '-Projection', '-Lavabender', '-Metalbender', '-Chiblocker', '-Sandbender', '-Swordmaster', '-Kyoshi Warrior', '-Chireader', '-Seismic Sense')
        await ctx.channel.send("{} has chosen the element of water!".format(await internals.user(ctx)))

    #STOP water

    #START fire

    @commands.command(description='fire custom command')
    async def fire(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Firebender', '-Earthbender', '-Waterbender', '-Airbender', '-Nonbender', '-Healer', '-Bloodbender', '-Weightless', '-Breathbender', '-Projection', '-Lavabender', '-Metalbender', '-Chiblocker', '-Sandbender', '-Swordmaster', '-Kyoshi Warrior', '-Spiritbender', '-Seismic Sense')
        await ctx.channel.send("{} has chosen the element of fire!".format(await internals.user(ctx)))

    #STOP fire

    #START earth

    @commands.command(description='earth custom command')
    async def earth(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Earthbender', '-Airbender', '-Waterbender', '-Firebender', '-Nonbender', '-Lightningbender', '-Combustionbender', '-Healer', '-Bloodbender', '-Weightless', '-Breathbender', '-Projection', '-Chiblocker', '-Swordmaster', '-Kyoshi Warrior', '-Spiritbender', '-Chireader')
        await ctx.channel.send("{} has chosen the element of earth!".format(await internals.user(ctx)))

    #STOP earth

    #START air

    @commands.command(description='air custom command')
    async def air(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Airbender', '-Earthbender', '-Waterbender', '-Firebender', '-Nonbender', '-Lightningbender', '-Combustionbender', '-Healer', '-Bloodbender', '-Lavabender', '-Metalbender', '-Chiblocker', '-Sandbender', '-Swordmaster', '-Kyoshi Warrior', '-Spiritbender', '-Chireader', '-Seismic Sense')
        await ctx.channel.send("{} has chosen the element of air!".format(await internals.user(ctx)))

    #STOP air
    
    
    #START breath
    
    @commands.command(description='breath custom command')
    @commands.has_role('Apprentice')
    @commands.has_role('Airbender')
    async def breath(self, ctx, *, args: str=""):
        await internals.role(ctx, '+Breathbender', '-Weightless', '-Projection')
        await ctx.channel.send("\r\n{} can now bend breath!".format(await internals.user(ctx)))
    
    #STOP breath
                                                                                                                                                                                                                    #END_FUNCS_MARKER - DO NOT REMOVE

def setup(bot):
    bot.add_cog(Customs(bot))
