#import discord
from icecream import ic

# internal functions for programs
async def mention_channel(ctx, channel):
    for tc in ctx.guild.text_channels:
        if tc.name == channel:
            return tc.mention
    # if here then the channel must not exist
    return "#" + channel


async def user(ctx):
    return ctx.author.mention


async def add_roles(ctx, *args):
    # adds all roles in args to the author
    # build list of Roles to add
    roles = []
    for r in ctx.guild.roles:
        if r.name in args and r not in roles:
            roles.append(r)
    await ctx.author.add_roles(roles, atomic=True)


async def remove_roles(ctx, *args):
    # removes all roles in args from author
    # build list of roles to remove
    roles = []
    for r in ctx.guild.roles:
        if r.name in args and r not in roles:
            roles.append(r)
    await ctx.author.remove_roles(roles, atomic=True)


async def role(ctx, *args):
    # adds roles prepended with a '+' and removes roles prepended with a '-'
    # first divide the passed roles into an add list and a remove list
    adds = []
    removes = []
    for r in args:
        if r[0] == '-':
            removes.append(r[1:])
        elif r[0] == '+':
            adds.append(r[1:])
        else:
            adds.append(r)
    #ic(adds, removes)
    # build list of Roles, start with member roles and add adding roles
    roles = ctx.author.roles
    for r in ctx.guild.roles:
        if r.name in adds and r not in roles:
            roles.append(r)

    #now take out the removing roles
    for r in ctx.guild.roles:
        if r.name in removes and r in roles:
            roles.remove(r)
    await ctx.author.edit(roles=roles)
