# parser.py
# steps through provided program string and identifies commands
# returns a string with commands replaced
#
# commands are surrounded by {}
#     to have {} show up in output, type \{\}
# to pass arguments to a command, put them after a :
#     example: {require: role1, role2}
#     the arg string will not inclue the ':' and will be whitespace stripped


import string
from random import randint


async def run_program(program, message_ctx, env):
	length = len(program)
	format_str_lst = []
	subs = 0
	# now look for commands
	i = 0
	while i < length:
		if program[i] == '\\' and i + 1 < length and program[i+1] in "-$#}{":
			format_str_lst.append(program[i+1])
			i += 2
			lenght = len(program)
			continue
		if program[i] == '{':
			# now get rest of command
			command, arg, i = await parse_command(program, i+1)
			result = await run_cmd(command, arg, message_ctx, env)
			if result == False:
				# this indicates permissions failure
				return 'You are not authorized to run this command'
			format_str_lst.append(result)
			i += 1
			subs += 1
			continue
		format_str_lst.append(program[i])
		i += 1
	format_str = ''.join(format_str_lst)
	return format_str.strip()


async def parse_command(program, i):
	command = ''
	arg = ''
	# iterate past white space
	while i < len(program) and program[i] in " \t\r\n":
		i += 1
	# fill in command until we hit whitespace, a :, or a }
	while i < len(program) and program[i] not in " \t\r\n:}":
		command = command + program[i]
		i += 1
	# skip any whitespace
	while i < len(program) and program[i] in " \t\n":
		i += 1
	# check if there is an arg string
	if program[i] == ':':
		# find end of arguments
		j = i
		while j < len(program) and program[j] not in "}":
			if program[j] == '\\':
				j += 1
			j += 1
		arg = program[i+1:j].strip()
		i = j
	else:
		while i < len(program) and program[i] not in "}":
			i += 1

	return command, arg, i


async def sub_msg_vals(program, msg_lst, msg):
	# substitutes $N for msg_lst[N][0]
	# if N > len(msg)-1 then does nothing
	# ignores \$
	# if $N+ then will substitute for entire msg starting at Nth value
	# ignores \+
	i = 0
	l = len(msg)
	lp = len(program)
	while i < lp:
		if program[i] == '\\':
			i += 2
			continue
		if program[i] == '$':
			j = i + 1
			n = ''
			while j < lp and program[j] in string.digits:
				n = n + program[j]
				j += 1
			if int(n) >= l:
				i += len(n)
				continue
			if j < lp and program[j] == '+':
				program = program[:i] + msg[msg_lst[int(n)][1]:] + program[j+1:]
				i += len(msg[msg_lst[int(n)][1]:])
				continue
			else:
				program = program[:i] + msg_lst[int(n)][0] + program[j:]
				i += len(msg_lst[int(n)][0])
				continue
		else:
			i += 1
		lp = len(program)
	return program


async def mention_channel(message, program):
	# replace any channel names with a mentionable string for that channel
	raw_channels = message.guild.channels
	channels = {}
	for rc in raw_channels:
		channels[rc.name] = rc
	lp = len(program)
	i = 0
	while i < lp:
		if program[i] == '\\':
			i += 2
			continue
		if program[i] == '#':
			j = i + 1
			name = ''
			while j < lp and (program[j] in string.ascii_letters or program[j] in string.digits or program[j] == '_'):
				name = name + program[j]
				j += 1
			print('channel name:',name)
			if name not in channels:
				i = j
				continue
			else:
				program = program[:i] + channels[name].mention + program[j:]
				i += len(channels[name].mention)
				lp = len(program)
				continue
		else:
			i += 1
	return program


async def split_msg(message):
	# splits message based on whitespace
	# returns list of tuples
	# tuples have form (val, index)
	#     where index is where val[0] is in message
	msg = []
	i = 0
	l = len(message)
	while i < l:
#		print('outer', i)
		# step past whitespace
		while i < l and message[i] in ' \t\r\n':
#			print('first', i)
			i += 1
		# make sure there's more to parse
		if i == l:
			break
		# now get this value
		# ignore escaped characters
		# if a " is encountered, continue until the next one is found
		msg.append(['',i])
		while i < l and message[i] not in " \t\r\n":
#			print('second', i)
			if message[i] == '\\':
				i += 2
				continue
			if message[i] == '"':
				# suck up everything until next " or end
				i += 1
				while i < l and message[i] != '"':
#					print('inner', i)
					if message[i] == '\\':
						i += 2
					i += 1
				i += 1
			else:
				i += 1
		msg[-1][0] = message[msg[-1][1]:i]
	return msg


async def run_cmd(com, arg, message_ctx, env):
	# checks that com is a valid command
	# cleans com and runs
	# if first character of com is command prefix, runs command from global environment rather than passed environment
	not_found_msg = "{} doesn't exist in {} scope"
	scope = 'local'
	if com[0] == COMMAND_PREFIX:
		com = com[1:]
		env = GLOBAL_ENV
		scope = 'global'
	if com in builtins:
		env = builtins
		scope = builtins
	if com not in env:
		return not_found_msg.format(com, scope)
	retVal = await env[com](message_ctx, arg)
	if retVal == None:
		return ''
	else:
		return retVal


async def get_cmd(message):
	i = 1
	l = len(message)
	while i < l and (message[i] in string.ascii_letters or message[i] in string.digits or message[i] == '_'):
		i += 1
	return message[1:i]


async def orchestate_program_exec(program, message, message_ctx, environment):
	argv = await split_msg(message)
	prog = await sub_msg_vals(program, argv, message)
	prog = await mention_channel(message_ctx, program)
	retVal = await run_program(prog, message_ctx, environment)
	if retVal == '':
		return None
	else:
		return retVal


def test_parse():
	prog = "{com1}\n{com2: arg1, arg2}\nRegular old text and a {  com3 : \t arg1}"
	fstr, coms = parse(prog)
	for tup in coms:
		print(tup[0], '|', tup[1])
	print()
	print(fstr)


def test_split_msg():
	message = '!test arg1 arg2 "arg3\\" arg4"'
	message = input('>')
	msg = split_msg(message)
	for p in msg:
		print(p[0], '|', p[1])


def com1(args):
	return ''


def com2(args):
	return ''


def com3(args):
	return args[0]


async def ban(message, args):
    msg = await split_msg(message.content)
    if len(msg) < 2 and args == '':
        await message.channel.send('who to ban, though?')
        return
    elif args == '':
        print(message.content[msg[1][1]:])
        args = message.content[msg[1][1]:]
    if randint(0,100) < 10:
    	await message.channel.send('ban hammer dropped on ' + message.author.mention)
    else:
	    await message.channel.send('ban hammer dropped on ' + ', '.join(args.split(' ')))


environment = {
	'com1': com1,
	'com2': com2,
	'com3': com3
}
COMMAND_PREFIX = '-'
GLOBAL_ENV = {}
builtins = {}

#print(parse("{com1}\n{com2: arg1, arg2}\nRegular old text and a {  com3 : \t arg1}", environment))
# message = "!ban john suse howard bob"
# program = "{!ban: $1 $3 $2 $1 $3+}"
# print(run_program(sub_msg_vals(program, split_msg(message), message), environment))
# print(split_msg(message))
# print(sub_msg_vals(program, split_msg(message), message))
